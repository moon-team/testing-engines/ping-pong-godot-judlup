extends Node2D

var player
var computer 
var ball2
var scoreP = 0
var scoreC = 0
var iniciar
func _ready():
	player = get_node("player")
	computer = get_node("player2")		
	ball2 = get_node("ball2")	
	get_node("scorePlayer").set_text(str(scoreP));
	get_node("scoreComputer").set_text(str(scoreC));
	Input.set_mouse_mode( Input.MOUSE_MODE_VISIBLE)	
	set_process(true)	

func _process(delta):
	if ball2.get_position().x <= player.get_position().x :
		ball2.set_process(false)
		ball2.set_position(Vector2(260,50))
		if scoreC == 5:
			scoreC = 5
		else:
			scoreC += 1
		get_node("scoreComputer").set_text(str(scoreC))
	elif ball2.get_position().x >= computer.get_position().x  :
		ball2.set_process(false)
		ball2.set_position(Vector2(260,50))
		if scoreP == 5:
			scoreP = 5
		else:
			scoreP += 1
		get_node("scorePlayer").set_text(str(scoreP))

	if scoreP == 5 :
		set_process(false)
		get_node("tablero").set_text("El ganador es Player 1")
		
	if scoreC == 5 :
		set_process(false)
		get_node("tablero").set_text("El ganador es Player 2")
		
func _input(event):	
	if event.is_action_pressed("ui_accept"):
		get_node("tablero").set_text("")
		scoreP = 0
		scoreC = 0
		get_node("scorePlayer").set_text(str(0))
		get_node("scoreComputer").set_text(str(0))
		ball2.set_position(Vector2(260,50))
		set_process(true)

