extends KinematicBody2D

var speed = 1000
var bounce_count = 0
var velocity = Vector2()

# Called when the node enters the scene tree for the first time.
func _ready():	
	set_process(false)
	#if Input.is_action_pressed("ui_up") || Input.is_action_pressed("mouse_left") :
	#	velocity = Vector2(speed, 0).rotated(45)		

func _process(delta):
	var collision = self.move_and_collide(velocity * delta)
	if collision :
		velocity = velocity.bounce(collision.normal)	
	
func _input(event):		
	if event.is_action_pressed("ui_jugar") :
		set_process(true)
		velocity = Vector2(speed, 0).rotated(45)		
