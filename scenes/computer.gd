extends RigidBody2D


func _ready():
	set_process(true)

func _process(delta):
	pass
	
func _input(event):	
	if event.as_text() == 'W' :				
		self.set_position(Vector2(self.get_position().x, self.get_position().y + 10 ))
	
	if event.as_text() == 'S' :		
		self.set_position(Vector2(self.get_position().x, self.get_position().y - 10 ))
